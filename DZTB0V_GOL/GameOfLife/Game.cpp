#include <Windows.h>
#include "Game.h"

#include <vector>
#include <random>
#include <iostream>

void gotoXY(int x, int y)
{
	HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD CursorPosition;
	CursorPosition.X = x;
	CursorPosition.Y = y;
	SetConsoleCursorPosition(console, CursorPosition);
}

std::ostream& operator<<(std::ostream& os, const Game& theGame)
{
	int width = theGame.getMap().size();
	int height = theGame.getMap()[0].size();
	int left = theGame.getLeft();
	int top = theGame.getTop();

	gotoXY(0, 0);
	
	for (int i = 0; i < width; ++i)
	{
		if ((theGame.getLeft() != 1) && (theGame.getTop() != 1))
		{
			gotoXY(theGame.getLeft(), theGame.getTop() + i);
		}

		for (size_t j = 0; j < height; j++)
		{
			os << (theGame.getMap()[i][j] == true ? "�" : "o");
		}
		os << std::endl;		
	}
	return os;
	
}

Game::Game(const int& height) {
	/*1 darab eg�sz bemenet: a j�t�kt�r legyen n�gyzet alak�,
	melynek �lhossza a bemeneti param�ter; minden egyes n�gyzetben
	10% val�sz�n�s�ggel legyen �l� sejt*/

	std::default_random_engine rE(rD());
	std::uniform_real_distribution<double> real_dist(0, 100.0);

	map1.reserve(height);
	map2.reserve(height);

	for (size_t i = 0; i < height; ++i) {

		std::vector<bool> v1;
		v1.reserve(height);

		for (size_t j = 0; j < height; ++j) {

			if (real_dist(rE) < 10) {
				v1.push_back(true);
			}
			else {
				v1.push_back(false);
			}
		}
		map1.push_back(v1);
		map2.push_back(v1);
	}
}

Game::Game(const int& height, const int& width) {
	/*2 darab eg�sz bemenet: a j�t�kt�r legyen t�glalap alak�,
	melynek magass�ga �s sz�less�ge a k�t bemeneti param�ter;
	minden egyes n�gyzetben 10% val�sz�n�s�ggel legyen �l� sejt*/

	std::default_random_engine rE(rD());
	std::uniform_real_distribution<double> real_dist(0, 100.0);

	map1.reserve(width);
	map2.reserve(width);

	for (size_t i = 0; i < width; ++i)
	{
		std::vector<bool> v1;
		v1.reserve(height);

		for (size_t j = 0; j < height; ++j)
		{
			if (real_dist(rE) < 10)
			{
				v1.push_back(true);
			}
			else
			{
				v1.push_back(false);
			}
		}
		map1.push_back(v1);
		map2.push_back(v1);
	}
}

Game::Game(const int& height, const double& consistence) {
	/*1 darab eg�sz �s 1 darab lebeg�pontos bemenet:
	a j�t�kt�r legyen n�gyzet alak�, melynek �lhossza
	az eg�sz t�pus� bemeneti param�ter; minden egyes n�gyzetben
	a lebeg�pontos �rt�k �ltal meghat�rozott val�sz�n�s�ggel legyen �l� sejt*/

	std::default_random_engine rE(rD());
	std::uniform_real_distribution<double> real_dist(0, 100.0);

	map1.reserve(height);
	map2.reserve(height);

	for (size_t i = 0; i < height; ++i)
	{
		std::vector<bool> v1;
		v1.reserve(height);

		for (size_t j = 0; j < height; ++j)
		{
			if (real_dist(rE) < consistence)
			{
				v1.push_back(true);
			}
			else
			{
				v1.push_back(false);
			}

		}
		map1.push_back(v1);
		map2.push_back(v1);
	}
}

Game::Game(const int& height, const int& width, const double& consistence) {
	/*2 darab eg�sz �s 1 darab lebeg�pontos bemenet:
	a j�t�kt�r legyen t�glalap alak�, melynek magass�ga �s sz�less�ge
	a k�t bemeneti param�ter; minden egyes n�gyzetben a lebeg�pontos �rt�k
	�ltal meghat�rozott val�sz�n�s�ggel legyen �l� sejt*/

	std::default_random_engine rE(rD());
	std::uniform_real_distribution<double> real_dist(0, 100.0);

	map1.reserve(width);
	map2.reserve(width);

	for (size_t i = 0; i < width; ++i)
	{
		std::vector<bool> v1;
		v1.reserve(height);

		for (size_t j = 0; j < height; ++j)
		{
			if (real_dist(rE) < consistence)
			{
				v1.push_back(true);
			}
			else
			{
				v1.push_back(false);
			}
		}
		map1.push_back(v1);
		map2.push_back(v1);
	}
}

Game::Game(const int& height, const int& width, const int& top, const int& left, const std::vector<std::vector<bool>>& pattern) {
	/*4 darab eg�sz (height, width, top, left) �s 1 darab vektorok vektora param�ter:
	a j�t�kt�r height magas �s width sz�les, a vektorok vektora �ltal meghat�rozott
	sejtmint�zat a j�t�kt�r (top, left) koordin�t�j� pontj�n�l kezd�d�en legyen beillesztve*/

	mLeft = left;
	mTop = top;

	map1.reserve(width);
	map2.reserve(width);

	for (size_t i = 0; i < width; ++i)
	{
		std::vector<bool> v1;
		v1.reserve(height);

		for (size_t j = 0; j < height; ++j)
		{
			v1.push_back(pattern[i][j]);
		}
		map1.push_back(v1);
		map2.push_back(v1);
	}
}

const std::vector<std::vector<bool>>& Game::getMap() const
{
	if (roundCounter % 2 == 0)
	{
		return map1;
	}
	else
	{
		return map2;
	}
}

const int& Game::getTop() const { return mTop; }
const int& Game::getLeft() const { return mLeft; }
const bool& Game::getAlive() const { return alive; }

void Game::Step() {

	if (roundCounter != 100)
	{
		if (roundCounter % 2 == 0)
		{
			Execute(map2, map1);
		}
		else
		{
			//itt kezd�nk �s ez lesz utolj�ra is megh�vva
			Execute(map1, map2);
		}
	}
}

void Game::Execute(std::vector<std::vector<bool>>& first, std::vector<std::vector<bool>>& second)
{
	int neighbour = 0;
	alive = false;

	for (int i = 0; i < first.size(); ++i)
	{
		for (int j = 0; j < first[0].size(); j++)
		{
			for (int k = -1; k < 2; k++)
			{
				for (int l = -1; l < 2; l++)
				{
					if (!(i + k < 0) && (!(j + l < 0)) && (!(i + k == first.size())) && (!(j + l == first[0].size())))
					{
						if (first[i + k][j + l] == true)
						{
							neighbour++;
						}
								
					}
				}
			}

			if (first[i][j] == true)
			{
				neighbour--;

				if (neighbour == 2 || neighbour == 3)
				{
					second[i][j] = true;
					alive = true;
				}
				else
				{
					second[i][j] = false;
				}
			}
			else 
			{
				if (neighbour == 3)
				{
					second[i][j] = true;
					alive = true;
				}
				else
				{
					second[i][j] = false;
				}
			}

			neighbour = 0;
		}
	}
}

void Game::Play()
{
	do {
		if (std::cin.get() == '\n')
		{
			roundCounter++;
			Step();
			if (getAlive())
			{
				std::cout << *this;
				gotoXY(mLeft, mTop + map1.size() + 1);
				std::cout << "Round : " << roundCounter << std::endl;
			}
			else
			{
				std::cout << *this;
				gotoXY(mLeft, mTop + map1.size() + 1);
				std::cout << "Game Over." << std::endl;
				roundCounter = 100;
			}
		}
	} while (roundCounter != 100);
}

