#pragma once

#include <vector>
#include <random>
#include <iostream>

class Game
{
public:
	Game(const int& height);

	Game(const int& height, const int& width);

	Game(const int& height, const double& consistence);

	Game(const int& height, const int& width, const double& consistence);

	Game(const int& height, const int& width, const int& top, const int& left, const std::vector<std::vector<bool>>& pattern);

	const std::vector<std::vector<bool>>& getMap() const;

	const int& getTop() const;
	const int& getLeft() const;
	const bool& getAlive() const;	

	void Play();

private:

	std::vector<std::vector<bool>> map1;
	std::vector<std::vector<bool>> map2;
	std::random_device rD;
	int roundCounter = 0;
	int mTop = 1;
	int mLeft = 1;
	bool alive;

	void Step();
	void Execute(std::vector<std::vector<bool>>& first, std::vector<std::vector<bool>>& second);	
};