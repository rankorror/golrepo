#include "Game.h"

#include <iostream>
#include <string>
#include <random>
#include <limits>

int main()
{
    std::vector<std::vector<bool>> testMap;
    testMap.resize(10, std::vector<bool>(10));
    testMap[1][5] = true;
    testMap[1][6] = true;
    testMap[2][3] = true;
    testMap[3][5] = true;
    testMap[4][6] = true;
    testMap[4][6] = true;
    testMap[5][8] = true;
    testMap[6][9] = true;
    testMap[6][8] = true;
    testMap[7][9] = true;
    testMap[8][9] = true;
    testMap[9][9] = true;

    Game g(30);
    //Game g(30, 40);
    //Game g(30, 50.0); // double param�ter: sz�zal�k 0-100-ig
    //Game g(30, 30, 50.0); // double param�ter: sz�zal�k 0-100-ig
    //Game g(10, 10, 5, 20, testMap); // p�ldak�nt megadott vektor alapj�n 10,10 kezdet� param�terekkel helyesen m�k�d�

    std::cout << "Press Enter to play!" << std::endl;

    g.Play();

    std::cin.get() == '\n';
}